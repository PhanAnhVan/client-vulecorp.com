import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { uploadFileService } from '../../../services/integrated/upload.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Globals } from '../../../globals';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-process',
    templateUrl: './process.component.html',
    styleUrls: ['./process.component.css']
})
export class ProcessComponent implements OnInit {

    public fm: FormGroup;

    public id: number = 0;

    private connect;

    public logo = new uploadFileService();

    public token: any = {

        process: "set/partner/process",

        getrow: "get/partner/getrow",
    }
    constructor(

        private fb: FormBuilder, private router: Router,

        public globals: Globals, private toastr: ToastrService,

        private routerAct: ActivatedRoute,
    ) {
        this.routerAct.params.subscribe(params => {
            this.id = +params['id'];
        })
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res['token']) {
                case "getrow":
                    let data = res['data'];
                    this.fmConfigs(data);
                    break;
                case "process":
                    let type = (res['status'] == 1) ? "success" : (res['status'] == 0 ? "warning" : "danger");
                    this.toastr[type](res['message'], type);

                    if (res['status'] == 1) {
                        setTimeout(() => {
                            this.router.navigate(['admin/partner/get-list']);
                        }, 2000);
                    }
                    break;
                default:
                    break;
            }
        });
    }


    ngOnInit() {
        if (this.id && this.id != 0) {
            this.globals.send({ path: this.token.getrow, token: 'getrow', params: { id: this.id } });
        } else {
            this.fmConfigs()
        }
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    fmConfigs(data: any = "") {

        data = typeof data === 'object' ? data : { status: 1, sex: 1, birth_date: new Date() };

        const imagesConfig = { path: this.globals.BASE_API_URL + 'public/partner/', data: data.logo ? data.logo : '' };

        this.logo._ini(imagesConfig);

        this.fm = this.fb.group({

            company: [data.company ? data.company : '', [Validators.required]],

            logo: data.logo ? data.logo : '',

            link: data.link ? data.link : '',

            orders: data.orders ? data.orders : null,

            type: data.type ? data.type : null,

            note: data.note ? data.note : '',

            status: (data.status && data.status == 1) ? true : false,
        })
    }

    onSubmit() {

        if (this.fm.valid) {

            let data = this.fm.value;

            data.logo = this.logo._get(true);

            data.status = data.status == true ? 1 : 0;

            this.globals.send({ path: this.token.process, token: "process", data: data, params: { id: this.id } });
        }
    }
}
