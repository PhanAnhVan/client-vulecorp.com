import { Component, OnDestroy, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ToastrService } from 'ngx-toastr';
import { Globals } from '../../../globals';
import { TableService } from '../../../services/integrated/table.service';
import { AlertComponent } from '../../modules/alert/alert.component';

@Component({
    selector: 'app-getlist',
    templateUrl: './getlist.component.html',
    styleUrls: ['./getlist.component.css']
})
export class GetlistComponent implements OnInit, OnDestroy {
    public id: number;
    modalRef: BsModalRef;
    public connect: any;
    public token: any = {
        getlist: "get/partner/getlist",
        remove: "set/partner/remove",
        checkHighLight: "set/partner/checkHighLight",
    }
    dismissedAlert: true;
    private cols = [
        { title: "lblStt", field: "index", show: true },
        { title: "partner.logo", field: "logo", show: true },
        { title: "partner.company", field: "company", show: true, filter: true },
        { title: 'lblMaker_date', field: 'maker_date', show: true, filter: true },
        { title: "lblAction", field: "action", show: true }
    ];
    public cwstable = new TableService()

    constructor(
        public globals: Globals,
        public toastr: ToastrService,
        public modalService: BsModalService
    ) {

        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res['token']) {
                case 'getlist':
                    this.cwstable.sorting = { field: "maker_date", sort: "DESC", type: "", };
                    this.cwstable._concat(res.data, true);
                    break;
                case 'remove':
                    let type = (res.status == 1) ? "success" : (res.status == 0 ? "warning" : "danger");
                    this.toastr[type](res.message, type);
                    if (res.status == 1) {
                        this.cwstable._delRowData(this.id);
                    }
                    break;
                case 'checkHighLight':
                    type = (res.status == 1) ? "success" : (res.status == 0 ? "warning" : "danger");
                    this.toastr[type](res.message, type);
                    if (res.status == 1) {
                        this.getList();
                    }
                    break;
                default:
                    break;
            }
        });


    }

    ngOnInit() {
        this.cwstable._ini({ cols: this.cols, data: [], keyword: 'partner', sorting: { field: "id", sort: "DESC", type: "number" } });
        this.getList();
    }
    getList() {
        this.globals.send({ path: this.token.getlist, token: 'getlist' });
    }
    onRemove(id: number, name: any) {
        this.id = id;
        this.modalRef = this.modalService.show(AlertComponent, { initialState: { messages: 'partner.remove', name: name } });
        this.modalRef.content.onClose.subscribe(result => {
            if (result == true) {
                this.globals.send({ path: this.token.remove, token: 'remove', params: { id: id } });
            }
        });
    }
    ngOnDestroy() {
        this.connect.unsubscribe();
    }
    checkHighLight(id, type) {
        this.globals.send({ path: this.token.checkHighLight, token: 'checkHighLight', params: { id: id, type: type == 1 ? 0 : 1 } });
    }
}
