import { Component, Input, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalService } from "ngx-bootstrap/modal";
import { BsModalRef } from "ngx-bootstrap/modal/bs-modal-ref.service";
import { ToastrService } from 'ngx-toastr';
import { Globals } from '../../../globals';
import { TableService } from '../../../services/integrated/table.service';
import { AlertComponent } from '../../modules/alert/alert.component';

@Component({
    selector: 'app-getlist',
    templateUrl: './getlist.component.html',
})

export class GetlistComponent implements OnInit, OnDestroy {

    private id: number;

    public connect;

    public show: any;

    public token: any = {
        getlist: "get/product/getlist",
        remove: "set/product/remove",
        changePin: "set/product/changePin",
        changeHot: "set/product/changeHot",
        getlistGroup: "get/product/grouptype"
    }
    @Input("filter") filter: any;

    @Input("change") change: any;

    modalRef: BsModalRef;

    private cols = [
        { title: 'lblStt', field: 'index', show: false },
        { title: 'lblName', field: 'name', show: true, filter: true },
        { title: 'lblImages', field: 'images', show: true },
        { title: 'products.nameGroup', field: 'namegroup', show: true },
        { title: 'lblMaker_date', field: 'maker_date', show: true, filter: true },
        { title: '#', field: 'action', show: true },
        { title: "", field: "status", show: true },
    ];

    public tableproduct = new TableService();

    constructor(
        private modalService: BsModalService,

        public toastr: ToastrService,

        public router: Router,

        public globals: Globals
    ) {
        this.connect = this.globals.result.subscribe((res: any) => {

            switch (res.token) {

                case "getlistproduct":
                    this.tableproduct.sorting = { field: "maker_date", sort: "DESC", type: "" };
                    this.tableproduct._concat(res.data, true);
                    break;

                case "removeproduct":
                    let type = (res['status'] == 1) ? "success" : (res['status'] == 0 ? "warning" : "danger");
                    this.toastr[type](res['message'], type);
                    if (res['status'] == 1) {
                        this.tableproduct._delRowData(this.id);
                        this.globals.send({ path: this.token.getlistGroup, token: 'getlistproductgroup', params: { type: 3 } });
                    }

                    break;

                case "changePin":
                case "changeHot":
                    type = res.status == 1 ? "success" : res.status == 0 ? "warning" : "danger";
                    this.toastr[type](res.message, type);

                    if (res.status == 1) {
                        this.globals.send({ path: this.token.getlist, token: 'getlistproduct' });
                    }
                    break;

                default:
                    break;
            }
        });


    }

    ngOnInit() {
        this.getlist();
    }
    ngOnDestroy() {
        this.connect.unsubscribe();
    }
    getlist = () => {
        this.globals.send({ path: this.token.getlist, token: 'getlistproduct' });
        this.tableproduct._ini({ cols: this.cols, data: [], count: 50 });
    }
    ngOnChanges(e: SimpleChanges) {

        if (typeof this.filter === 'object') {

            let value = Object.keys(this.filter);

            if (value.length == 0) {

                this.tableproduct._delFilter('page_id');

            } else {
                this.tableproduct._setFilter('page_id', value, 'in', 'number');
            }
        }
    }
    onClose() {
        this.modalRef.hide();
    }
    onRemove(item: any): void {

        this.id = item.id;

        this.modalRef = this.modalService.show(AlertComponent, { initialState: { messages: 'products.remove', name: item.name } });

        this.modalRef.content.onClose.subscribe(result => {

            if (result == true) {

                this.globals.send({ path: this.token.remove, token: 'removeproduct', params: { id: item.id } });
            }
        });
    }
    changePin = (id, pin) => {

        this.globals.send({ path: this.token.changePin, token: 'changePin', params: { id: id, pin: pin == 1 ? 0 : 1 } });
    }

    changeHot = (id, hot) => {

        this.globals.send({ path: this.token.changeHot, token: 'changeHot', params: { id: id, hot: hot == 1 ? 0 : 1 } });
    }
}

