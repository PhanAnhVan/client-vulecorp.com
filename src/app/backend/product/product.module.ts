import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { CKEditorModule } from "ckeditor4-angular";
import { AlertModule } from 'ngx-bootstrap/alert';
import { BsModalRef, ModalModule } from "ngx-bootstrap/modal";
import { TabsModule } from "ngx-bootstrap/tabs";
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { TypeaheadModule } from "ngx-bootstrap/typeahead";
import { numberModule } from "src/app/services/symbol";
import { GetlistComponent } from "./getlist/getlist.component";
import { GroupProductGetlistComponent } from "./group/group-product.component";
import { MainComponent } from "./main/main.component";
import { ProcessProductComponent } from "./process/process.component";

const appRoutes: Routes = [
    { path: "", redirectTo: "get-list" },
    { path: "get-list", component: MainComponent },
    { path: "insert", component: ProcessProductComponent },
    { path: "update/:id", component: ProcessProductComponent },
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(appRoutes),
        ModalModule.forRoot(),
        AlertModule.forRoot(),
        TabsModule.forRoot(),
        TooltipModule.forRoot(),
        TranslateModule,
        CKEditorModule,
        TypeaheadModule.forRoot(),
        numberModule
    ],
    providers: [BsModalRef],
    declarations: [
        GetlistComponent,
        GroupProductGetlistComponent,
        ProcessProductComponent,
        MainComponent,
    ],
})
export class ProductModule { }
