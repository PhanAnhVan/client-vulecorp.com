import { Component, OnDestroy, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { Subscription } from "rxjs";
import { Globals } from "../../../globals";
import { LinkService } from "../../../services/integrated/link.service";
import { TagsService } from "../../../services/integrated/tags.service";
import { uploadFileService } from "../../../services/integrated/upload.service";

@Component({
    selector: "app-process",
    templateUrl: "./process.component.html",
    styleUrls: ['./process.component.css']
})
export class ProcessProductComponent implements OnInit, OnDestroy {
    private connect: Subscription;

    public id: number = 0;
    public fm: FormGroup;
    public price: FormGroup;
    public seo: FormGroup;

    private token: any = {
        getrow: "get/product/getrow",
        getlistproductgroup: "get/pages/grouptype",
        getListOrigin: "get/origin/getlist",
        getListBrand: "get/brand/getlist",
        process: "set/product/process",
        updateSeo: "set/product/updateSeo",
        updatePrice: "set/product/updatePrice"
    };

    public data = { origin: [], brand: [], group: [], type: [] };
    public skip = false;
    public images = new uploadFileService();
    public listimages = new uploadFileService();

    constructor(
        private routerAct: ActivatedRoute,
        public router: Router,
        public globals: Globals,
        private link: LinkService,
        private fb: FormBuilder,
        public tags: TagsService,
        private toastr: ToastrService,
    ) {
        this.routerAct.params.subscribe((params) => {
            this.id = +params["id"];

            if (this.id && this.id > 0) {
                this.get();
            } else {
                this.fmConfigs();
                this.priceConfigs();
                this.seoConfigs();
                this.productImages.set();
            }
        });
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case "getrow":
                    let data = res.data;
                    this.fmConfigs(data);
                    this.priceConfigs(data);
                    this.seoConfigs(data);
                    this.productImages.set(data);
                    break;

                case "getListBrand":
                    if (res.status === 1) {
                        this.data.brand = res.data;
                    }
                    break;

                case "getListOrigin":
                    this.data.origin = res.data;
                    break;

                case "getlistproductgroup":
                    this.data.group = res.data;
                    break;

                case "processProduct":
                    if (this.id == 0) {
                        this.id = res.data;
                        setTimeout(() => {
                            this.router.navigate([this.globals.admin + '/products/update/' + this.id]);
                        }, 100);
                    }

                case "updateSeo":
                case 'updatePrice':
                case "updateImages":
                case "updateProduct":
                    let type = (res.status == 1) ? "success" : (res.status == 0 ? "warning" : "danger");
                    this.toastr[type](res.message, type);
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.globals.send({ path: this.token.getListBrand, token: "getListBrand" });
        this.globals.send({ path: this.token.getListOrigin, token: "getListOrigin" });
        this.globals.send({ path: this.token.getlistproductgroup, token: "getlistproductgroup", params: { type: 3 }, });
    }

    get() {
        this.globals.send({ path: this.token.getrow, token: 'getrow', params: { id: this.id } });
    }

    ngOnDestroy() { this.connect.unsubscribe(); }

    fmConfigs(item: any = "") {

        item = typeof item === 'object' ? item : { status: 1, page_id: 0, origin_id: 0, brand_id: 0 };

        this.fm = this.fb.group({
            name: [item.name ? item.name : '', [Validators.required]],
            link: [item.link ? item.link : '', [Validators.required]],
            page_id: [item.page_id ? item.page_id : 0, [Validators.required]],
            brand_id: [item.brand_id ? item.brand_id : 0, [Validators.required]],
            origin_id: [item.origin_id ? item.origin_id : 0, [Validators.required]],
            info: item.info ? item.info : '',
            hot: +item.hot ? +item.hot : 0,
            detail: item.detail ? item.detail : '',
            status: (item.status && +item.status == 1) ? true : false,
        });
    }

    onSubmit() {
        if (this.fm.valid) {
            const obj = this.fm.value;

            obj.status = obj.status === true ? 1 : 0;

            this.globals.send({ path: this.token.process, token: 'processProduct', data: obj, params: { id: this.id || 0 } });
        }
    }

    priceConfigs(item: any = "") {
        item = typeof item === 'object' ? item : { inventory: 'Còn hàng' };
        this.price = this.fb.group({
            price_sale: [item.price_sale ? +item.price_sale : 0],
            inventory: item.inventory ? item.inventory : '',
            price: [item.price ? +item.price : 0],
            vat: item.vat ? +item.vat : 0,
        });
    }

    updatePrice = () => {
        const obj = this.price.value;

        obj.price = this.globals.price.format(obj.price);
        obj.price_sale = this.globals.price.format(obj.price_sale);

        this.globals.send({ path: this.token.updatePrice, token: 'updatePrice', data: obj, params: { id: this.id || 0 } });
    }

    seoConfigs(item: any = "") {
        item = typeof item === 'object' ? item : { status: 1 };

        this.seo = this.fb.group({
            description: item.description ? item.description : '',
        });

        this.tags._set(item.keywords ? JSON.parse(item['keywords']) : "");
    }


    updateSeo = () => {

        const obj = this.seo.value;

        obj.keywords = this.tags._get();

        this.globals.send({ path: this.token.updateSeo, token: 'updateSeo', data: obj, params: { id: this.id || 0 } });
    }

    public productImages = {
        tokenProcess: "set/product/updateImages",

        set: (item: any = "") => {
            item = typeof item === "object" ? item : { images: "", listimages: "" };
            const imagesConfig = {
                path: this.globals.BASE_API_URL + "public/products/",
                data: item.images ? item.images : "",
            };
            this.images._ini(imagesConfig);
            const listimagesConfig = {
                path: this.globals.BASE_API_URL + "public/products/",
                data: item.listimages ? item.listimages : "",
                multiple: true,
            };
            this.listimages._ini(listimagesConfig);
        },

        process: () => {
            if (this.id > 0) {
                let data: any = {};
                data.images = this.images._get(true);
                data.listimages = this.listimages._get(true);

                this.globals.send({
                    path: this.productImages.tokenProcess,
                    token: "updateImages",
                    data: data,
                    params: { id: this.id > 0 ? this.id : 0 },
                });
            }
        },
    };

    onChangeLink(e: { target: { value: any; }; }) {
        const url = this.link._convent(e.target.value);
        this.fm.value.link = url;
    }
}
