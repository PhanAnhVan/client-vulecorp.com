import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Globals } from '../../../globals';
@Component({
    selector: 'app-changespassword',
    templateUrl: './changespassword.component.html',
    styleUrls: ['./changespassword.component.css']
})
export class ChangespasswordComponent implements OnInit {
    public id: number;
    public item: any = {}
    fm: FormGroup;
    public connect;
    public token: any = {
        changespassword: "set/user/changepassword",
        getrow: "get/user/getrow"
    }
    constructor(
        public router: Router,
        public toastr: ToastrService,
        private routerAct: ActivatedRoute,
        public fb: FormBuilder,
        public globals: Globals,
        public translate: TranslateService
    ) {

        this.fm = this.fb.group({
            password: ['', [Validators.required, Validators.minLength(8)]]
        })


        this.routerAct.params.subscribe(params => {
            this.id = +params['id'];
            this.globals.send({ path: this.token.getrow, token: "getrow", params: { id: this.id } });
        })
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res['token']) {
                case "getrow":

                    this.item = res.data;

                    break;
                case "changespassword":
                    let type = (res['status'] == 1) ? "success" : (res['status'] == 0 ? "warning" : "danger");

                    this.toastr[type](res['message'], type);

                    if (res['status'] == 1) {

                        setTimeout(() => {

                            if (res.data == this.id) {

                                this.globals.USERS.remove()
                            }
                            this.router.navigate(['admin/user/get-list']);

                        }, 1000);
                    }
                    break;
                default:
                    break;
            }
        });
    }

    ngOnInit() {
    }
    onSubmit() {
        let data = this.fm.value;

        this.globals.send({ path: this.token.changespassword, token: "changespassword", data: data, params: { id: this.id || 0 } });
    }
}
