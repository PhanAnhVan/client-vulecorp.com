import { Component, OnInit, ViewContainerRef, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { Globals } from '../../../globals';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
})
export class EmailComponent implements OnInit, OnDestroy {
  fm: FormGroup;
  public connect: any;
  public token: any = {
    process: "set/settings/email",
    getrow: "get/settings/emailgetrow"
  }
  constructor(
    public fb: FormBuilder,
    public router: Router,
    public toastr: ToastrService,
    public globals: Globals,
  ) {


    this.connect = this.globals.result.subscribe((res: any) => {
      switch (res['token']) {
        case 'getrowSettingEmail':
          let data = res['data'];
          this.fm = this.fb.group({
            local: data.local ? data.local : '',
            protocol: data.protocol ? data.protocol : '',
            port: data.port ? data.port : '',
            password: [data.password ? data.password : '', [Validators.required]],
            mail: [data.mail ? data.mail : '', [Validators.required, Validators.pattern(/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i)]],
          });
          break;
        case 'processSettingEmail':
          let type = (res['status'] == 1) ? "success" : (res['status'] == 0 ? "warning" : "danger");
          this.toastr[type](res['message'], type);
          if (res['status'] == 1) {
            this.getRow()
          } break;
        default:
          break;
      }
    });
  }
  ngOnDestroy() {
    this.connect.unsubscribe()
  }
  ngOnInit() {
    this.getRow()
  }
  getRow() { this.globals.send({ path: this.token.getrow, token: 'getrowSettingEmail' }); }

  onSubmit() {
    let data = this.fm.value;
    this.globals.send({ path: this.token.process, token: 'processSettingEmail', data: data });
  }
}
