import { Component, OnDestroy, OnInit } from '@angular/core';
import { BsModalService } from "ngx-bootstrap/modal";
import { BsModalRef } from "ngx-bootstrap/modal/bs-modal-ref.service";
import { ToastrService } from 'ngx-toastr';
import { Globals } from '../../../../globals';
import { TableService } from '../../../../services/integrated/table.service';
import { AlertComponent } from '../../../modules/alert/alert.component';

@Component({
    selector: 'app-get-list',
    templateUrl: './get-list.component.html',
})
export class MenuGetListComponent implements OnInit, OnDestroy {

    public connect: any;

    modalRef: BsModalRef;

    public id: number = 0;

    public token: any = {
        getlist: "get/pagesgroup/getlist", //router lấy dữ liệu 

        remove: "set/pagesgroup/remove"
    }
    private cols = [
        { title: 'lblStt', field: 'index', show: true },
        { title: 'lblName', field: 'name', show: true, filter: true },
        { title: 'lblOrders', field: 'orders', show: false, filter: true },
        { title: 'lblMaker_date', field: 'maker_date', show: true, filter: true },
        { title: 'lblAction', field: 'action', show: true },
        { title: '#', field: 'status', show: true, filter: true },

    ];
    public table = new TableService()
    constructor(

        public globals: Globals,

        private modalService: BsModalService,

        public toastr: ToastrService,

    ) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res['token']) {
                case 'getListPagesGroup':
                    this.table._concat(res['data'], true);
                    break;
                case 'removePagesGroup':
                    let type = (res['status'] == 1) ? "success" : (res['status'] == 0 ? "warning" : "danger");
                    this.toastr[type](res['message'], type);
                    if (res['status'] == 1) {
                        this.table._delRowData(this.id)
                    }
                    break;
                default:
                    break;
            }
        });
    }
    ngOnInit() {
        this.getList();
        this.table._ini({ cols: this.cols, data: [], keyword: 'getListPagesGroup' });
    }
    getList() {
        this.globals.send({ path: this.token.getlist, token: 'getListPagesGroup' });
    }
    ngOnDestroy() {
        this.connect.unsubscribe();
    }
    onRemove(item: any): void {
        this.id = item.id;
        this.modalRef = this.modalService.show(AlertComponent, { initialState: { messages: 'pagesGroup.removePagesGroup', name: item.name } });
        this.modalRef.content.onClose.subscribe(result => {
            if (result == true) {
                this.globals.send({ path: this.token.remove, token: 'removePagesGroup', params: { id: item.id } });
            }
        });
    }

}
