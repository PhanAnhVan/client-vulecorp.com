import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { Globals } from '../../../../globals';

@Component({
    selector: 'app-process',
    templateUrl: './process.component.html',
})
export class OriginProcessComponent implements OnInit, OnDestroy {

    public id: number = 0;

    public connect;

    private token: any = {

        process: "set/origin/process",

        getrow: "get/origin/getrow"
    }

    fm: FormGroup;

    constructor(
        public fb: FormBuilder,
        private router: Router,
        private routerAct: ActivatedRoute,
        public toastr: ToastrService,
        private globals: Globals,
    ) {

        this.connect = this.globals.result.subscribe((res: any) => {

            switch (res.token) {

                case "processorigin":
                    let type = (res.status == 1) ? "success" : (res.status == 0 ? "warning" : "danger");
                    this.toastr[type](res.message, type);
                    if (res.status == 1) {
                        setTimeout(() => {
                            this.router.navigate([this.globals.admin + '/setting/origin/get-list']);
                        }, 100);
                    }
                    break;

                case "getroworigin":
                    this.fmConfigs(res.data);
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {

        this.routerAct.params.subscribe(params => {

            this.id = +params['id'];

            if (this.id && this.id != 0) {

                this.getRow();

            } else {

                this.fmConfigs();
            }
        })
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    getRow() {
        this.globals.send({ path: this.token.getrow, token: "getroworigin", params: { id: this.id } });
    }

    fmConfigs(item: any = "") {

        item = typeof item === 'object' ? item : { status: 1, parent_id: 0, orders: 0 };

        this.fm = this.fb.group({

            name: [item.name ? item.name : '', [Validators.required]],

            orders: +item.orders ? +item.orders : 0,

            note: item.note ? item.note : '',

            status: (item.status && +item.status == 1) ? true : false,

        })
    }

    onSubmit() {

        const obj = this.fm.value;

        obj.status === true ? obj.status = 1 : obj.status = 0;

        this.globals.send({ path: this.token.process, token: 'processorigin', data: obj, params: { id: this.id | 0 } });
    }
}
