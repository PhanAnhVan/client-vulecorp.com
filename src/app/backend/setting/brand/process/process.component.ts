import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { Globals } from 'src/app/globals';

@Component({
    selector: 'app-process',
    templateUrl: './process.component.html',
})
export class ProcessComponent implements OnInit, OnDestroy {
    private connect: Subscription;

    public id: number = 0;
    public fm: FormGroup;
    private token = {
        process: "set/brand/process",
        getrow: "get/brand/getrow"
    }

    constructor(
        public fb: FormBuilder,
        public router: Router,
        private toastr: ToastrService,
        public globals: Globals,
        private routerAct: ActivatedRoute
    ) {
        this.routerAct.params.subscribe(params => {
            this.id = +params.id
        })

        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res['token']) {
                case 'getrow':
                    let data = res['data'];
                    this.fmConfigs(data);
                    break;

                case 'processBrand':
                    let type = (res['status'] == 1) ? "success" : (res['status'] == 0 ? "warning" : "danger");
                    this.toastr[type](res['message'], type);
                    if (res['status'] == 1) {
                        setTimeout(() => {
                            this.router.navigate([this.globals.admin + '/setting/brand/get-list']);
                        }, 100);
                    }
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {
        if (this.id && this.id != 0) {
            this.getRow();
        } else {
            this.fmConfigs()
        }
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    getRow() {
        this.globals.send({
            path: this.token.getrow,
            token: 'getrow',
            params: { id: this.id }
        });
    }

    fmConfigs(item: any = "") {
        item = typeof item === 'object' ? item : { status: 1, type: 1 };
        this.fm = this.fb.group({
            name: [item.name ? item.name : '', [Validators.required]],
            note: item.note ? item.note : '',
            orders: item.orders ? item.orders : 0,
            status: (item.status && item.status == 1) ? true : false,
        });
    }

    onSubmit() {
        if (this.fm.valid) {
            var data = this.fm.value;
            data.status = data.status == true ? 1 : 0;
            this.globals.send({
                path: this.token.process,
                token: 'processBrand',
                data: data,
                params: { id: this.id || 0 }
            });
        }
    }
}
