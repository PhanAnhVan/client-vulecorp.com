import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

import { Globals } from '../../../globals';
import { uploadFileService } from '../../../services/integrated/upload.service';
import { TagsService } from '../../../services/integrated/tags.service';

@Component({
    selector: 'app-website',
    templateUrl: './website.component.html',
    providers: [uploadFileService, TagsService],
})
export class WebsiteComponent implements OnInit, OnDestroy {

    fm: FormGroup;
    public connect: any;
    public token: any = {
        process: "set/title/company",
        getrow: "get/title/getrowcompany"
    }
    public logo: uploadFileService;
    public shortcut: uploadFileService;
    public logo_white: uploadFileService;
    constructor(
        public globals: Globals,
        public fb: FormBuilder,
        public toastr: ToastrService,
        public tags: TagsService,
    ) {

        this.connect = this.globals.result.subscribe((res: any) => {

            switch (res['token']) {

                case 'getrow':
                    let data: any = res['data'];
                    this.logo = new uploadFileService();
                    const logoConfig = { path: this.globals.BASE_API_URL + 'public/website/', data: data.logo };
                    this.logo._ini(logoConfig);
                    this.logo_white = new uploadFileService();
                    const logoWhiteConfig = { path: this.globals.BASE_API_URL + 'public/website/', data: data.logo_white };
                    this.logo_white._ini(logoWhiteConfig);
                    this.shortcut = new uploadFileService();
                    const shortConfig = { path: this.globals.BASE_API_URL + 'public/website/', data: data.shortcut };
                    this.shortcut._ini(shortConfig);
                    this.tags._set(data.keywords);
                    this.fm = this.fb.group({
                        name: [data.name, [Validators.required]],
                        description: data.description,
                    });
                    break;

                case 'processWebsite':
                    let type = (res['status'] == 1) ? "success" : (res['status'] == 0 ? "warning" : "danger");
                    this.toastr[type](res['message'], type);
                default:
                    break;
            }
        });
    }
    ngOnInit() {
        this.globals.send({ path: this.token.getrow, token: 'getrow' });
    }
    ngOnDestroy() {
        this.connect.unsubscribe()
    }
    onSubmit() {
        let data: any = this.fm.value;
        data.logo = this.logo._get(true);
        data.logo_white = this.logo_white._get(true);
        data.shortcut = this.shortcut._get(true);
        data.keywords = this.tags._get();
        data.status = 1;
        this.globals.send({ path: this.token.process, token: 'processWebsite', data: data });
    }
}
