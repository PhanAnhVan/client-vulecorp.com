import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { uploadFileService } from '../../../../services/integrated/upload.service';
import { Globals } from '../../../../globals';

@Component({
    selector: 'app-process',
    templateUrl: './process.component.html',
})
export class ProcessComponent implements OnInit, OnDestroy {

    public id: number = 0;
    public fm: FormGroup;
    private connect;
    private token: any = {
        process: "set/slide/process",
        getrow: "get/slide/getrow"
    }
    public images = new uploadFileService();
    public position = [
        { value: 1, name: "Left" },
        { value: 2, name: "Center" },
        { value: 3, name: "Right" },
        { value: 4, name: "Hiden" },
    ]
    constructor(
        public fb: FormBuilder,
        public router: Router,
        private toastr: ToastrService,
        public globals: Globals,
        private routerAct: ActivatedRoute
    ) {

        this.routerAct.params.subscribe(params => {
            this.id = +params.id
        })

        this.connect = this.globals.result.subscribe((response: any) => {

            switch (response['token']) {

                case 'getrow':
                    let data = response['data'];

                    this.fmConfigs(data);
                    break;

                case 'processBanner':
                    let type = (response['status'] == 1) ? "success" : (response['status'] == 0 ? "warning" : "danger");
                    this.toastr[type](response['message'], type);
                    if (response['status'] == 1) {
                        setTimeout(() => {
                            this.router.navigate([this.globals.admin + '/setting/slide/get-list']);
                        }, 100);
                    }
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {
        if (this.id && this.id != 0) {
            this.getRow();
        } else {
            this.fmConfigs()
        }
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    getRow() {
        this.globals.send({ path: this.token.getrow, token: 'getrow', params: { id: this.id } });
    }

    fmConfigs(item: any = "") {
        item = typeof item === 'object' ? item : { status: 1, type: 1, position: 0 };
        this.fm = this.fb.group({
            id: item.id ? item.id : '',
            name: [item.name ? item.name : '', [Validators.required]],
            link: item.link ? item.link : '',
            orders: +item.orders ? +item.orders : 0,
            type: +item.type ? +item.type : '',
            title: item.title ? item.title : '',
            position: item.position ? item.position : 0,
            description: item.description ? item.description : '',
            status: (item.status && item.status == 1) ? true : false,
        });

        const imagesConfig = { path: this.globals.BASE_API_URL + 'public/slides/', data: item.images ? item.images : '' };
        this.images._ini(imagesConfig);
    }

    changeUrlVideo(e) {
        e.target.value = +this.fm.value.type === 2 ? e.target.value.replace('watch?v=', 'embed/') : e.target.value;
    }

    onSubmit() {
        if (this.fm.valid) {
            var data: any = this.fm.value;
            data.images = this.images._get(true);
            data.status = data.status == true ? 1 : 0;
            if (+data.type == 2) {
                data.link = data.link.replace('watch?v=', 'embed/');
            } else {
                data.link = data.link.replace('embed/', 'watch?v=');
            }
            this.globals.send({ path: this.token.process, token: 'processBanner', data: data, params: { id: this.id || 0 } })
        }
    }
}
