import {
    Component,
    OnInit,
    ViewContainerRef,
    Input,
    OnChanges,
    SimpleChanges,
    OnDestroy,
} from "@angular/core";

import { TableService } from "../../../services/integrated/table.service";
import { BsModalService } from "ngx-bootstrap/modal";
import { BsModalRef } from "ngx-bootstrap/modal/bs-modal-ref.service";
import { ToastrService } from "ngx-toastr";

import { Globals } from "../../../globals";
import { AlertComponent } from "../../modules/alert/alert.component";

@Component({
    selector: "app-getlist",
    templateUrl: "./getlist.component.html",
})
export class GetlistComponent implements OnInit, OnChanges, OnDestroy {
    private id: number;

    public show;

    private name: any;

    modalRef: BsModalRef;

    @Input("filter") filter: any;

    @Input("change") change: any;

    public connect;

    public token: any = {
        getlist: "get/content/getlist",
        remove: "set/content/remove",
        changePin: "set/content/changepin",
        getlistGroup: "get/pages/grouptype",
    };

    private cols = [
        { title: "contents.name", field: "name", show: true, filter: true },

        { title: "lblGroup", field: "name_group", show: true, filter: true },

        { title: "lblMaker_date", field: "maker_date", show: true, filter: true },

        { title: "#", field: "action", show: true },

        { title: "", field: "status", show: true },
    ];

    public cstable = new TableService();

    constructor(
        public globals: Globals,

        private modalService: BsModalService,

        public toastr: ToastrService,

        vcr: ViewContainerRef
    ) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case "getlistcontent":
                    this.cstable.sorting = { field: "maker_date", sort: "DESC", type: "", };
                    this.cstable._concat(res.data, true);

                    break;
                case "removecontent":
                    let type = res.status == 1 ? "success" : res.status == 0 ? "warning" : "danger";
                    this.toastr[type](res.message, type);

                    if (res.status == 1) {
                        this.cstable._delRowData(this.id);
                        this.globals.send({ path: this.token.getlistGroup, token: 'contentgroup', params: { type: 4 } });
                    }
                    break;
                case "changepin":
                    let request = res.status == 1 ? "success" : res.status == 0 ? "warning" : "danger";
                    this.toastr[request](res.message, request);

                    if (res.status == 1) {
                        this.globals.send({ path: this.token.getlist, token: "getlistcontent" });
                    }
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.globals.send({ path: this.token.getlist, token: "getlistcontent" });

        this.cstable._ini({ cols: this.cols, data: [], keyword: "content", count: 50, });
    }
    ngOnDestroy() {
        this.connect.unsubscribe();
    }
    ngOnChanges(e: SimpleChanges) {
        if (typeof this.filter === "object") {
            let value = Object.keys(this.filter);

            if (value.length == 0) {
                this.cstable._delFilter("page_id");
            } else {
                this.cstable._setFilter("page_id", value, "in", "number");
            }
        }
    }
    onRemove(id: number, name: any) {
        this.id = id;

        this.modalRef = this.modalService.show(AlertComponent, {
            initialState: { messages: "contents.remove", name: name },
        });

        this.modalRef.content.onClose.subscribe((result) => {
            if (result == true) {
                this.globals.send({
                    path: this.token.remove,
                    token: "removecontent",
                    params: { id: id },
                });
            }
        });
    }

    pinContent = (id, pin) => {
        this.globals.send({ path: this.token.changePin, token: 'changepin', params: { id: id, pin: pin == 1 ? 0 : 1 } });
    }
}
