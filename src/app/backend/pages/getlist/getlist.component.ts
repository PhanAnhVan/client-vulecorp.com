import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalService } from "ngx-bootstrap/modal";
import { BsModalRef } from "ngx-bootstrap/modal/bs-modal-ref.service";
import { ToastrService } from 'ngx-toastr';
import { Globals } from '../../../globals';
import { TableService } from '../../../services/integrated/table.service';
import { AlertComponent } from '../../modules/alert/alert.component';

@Component({
    selector: 'app-getlist',
    templateUrl: './getlist.component.html',
})
export class GetlistComponent implements OnInit, OnDestroy {

    public connect: any;

    public type: number = 0;

    modalRef: BsModalRef;

    public id: number;

    public token: any = {

        getlist: "get/pages/getlist", //router lấy dữ liệu 

        remove: "set/pages/remove",

        changestatus: "set/pages/changestatus",
    }
    private cols = [
        { title: 'lblStt', field: 'index', show: true },
        { title: 'lblName', field: 'name', show: true, filter: true },
        { title: 'lblparent_name', field: 'parent_name', show: true, filter: true },
        { title: 'lblMaker_date', field: 'maker_date', show: true, filter: true },
        { title: '#', field: 'action', show: true },

    ];
    public cwstable = new TableService();

    constructor(

        private modalService: BsModalService,

        public router: Router,

        public toastr: ToastrService,

        public globals: Globals
    ) {

        this.cwstable._ini({ cols: this.cols, data: [], keyword: 'getListPages', count: 50 });

        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res['token']) {
                case 'getListPages':

                    this.cwstable.data = [];

                    this.cwstable.sorting = { field: "maker_date", sort: "DESC", type: "" };

                    this.cwstable._concat(res['data'], true);

                    break;
                case 'removePages':

                    let type = (res['status'] == 1) ? "success" : (res['status'] == 0 ? "warning" : "danger");

                    this.toastr[type](res['message'], type);

                    if (res['status'] == 1) {
                        this.cwstable._delRowData(this.id)
                    }
                    break;
                case 'changestatus':
                    let request = res.status == 1
                        ? "success"
                        : res.status == 0
                            ? "warning"
                            : "danger";

                    this.toastr[request](res.message, request);

                    if (res.status == 1) {
                        this.getList()
                    }
                    break;
                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.getList()
    }
    getList() {

        this.globals.send({ path: this.token.getlist, token: 'getListPages', params: { type: 2 } });
    }
    ngOnDestroy() {
        this.connect.unsubscribe();
    }
    onRemove(item: any): void {

        this.id = item.id;

        this.modalRef = this.modalService.show(AlertComponent, { initialState: { messages: 'pages.removePages', name: item.name } });

        this.modalRef.content.onClose.subscribe(result => {

            if (result == true) {

                this.globals.send({ path: this.token.remove, token: 'removePages', params: { id: item.id } });
            }
        });
    }
    changeStatus = (id, status) => {
        this.globals.send({ path: this.token.changestatus, token: 'changestatus', params: { id: id, status: status == 1 ? 0 : 1 } });
    }
}

