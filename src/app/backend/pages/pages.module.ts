import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { CKEditorModule } from 'ckeditor4-angular';
import { AlertModule } from 'ngx-bootstrap/alert';
import { ModalModule } from "ngx-bootstrap/modal";
import { GetlistComponent } from './getlist/getlist.component';
import { ProcessComponent } from './process/process.component';

const appRoutes: Routes = [
	{ path: '', redirectTo: 'get-list' },
	{ path: 'get-list', component: GetlistComponent },
	{ path: 'insert', component: ProcessComponent },
	{ path: 'update/:id', component: ProcessComponent },
]

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		RouterModule.forChild(appRoutes),
		ModalModule.forRoot(),
		AlertModule.forRoot(),
		TranslateModule,
		CKEditorModule
	],
	declarations: [GetlistComponent, ProcessComponent]
})
export class PagesModule { }
