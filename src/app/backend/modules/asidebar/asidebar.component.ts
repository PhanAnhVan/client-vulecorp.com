import { Component, OnInit, Input } from '@angular/core';
import { Globals } from '../../../globals';

@Component({
    selector: 'admin-asidebar',
    templateUrl: './asidebar.component.html',
    styleUrls: ['./asidebar.component.css']
})
export class AsidebarComponent implements OnInit {

    public connect;

    @Input('opened') opened: boolean;

    public token: any = {
        getUnRead: "get/dashboard/getUnRead",
    }

    public data = [];

    constructor(public globals: Globals,) {

        this.connect = this.globals.result.subscribe((res: any) => {

            switch (res.token) {
                case "getUnRead":
                    this.data = res.data;
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {
        // this.globals.send({ path: this.token.getUnRead, token: 'getUnRead' });
    }

    ngOnDestroy() {
        if (this.connect) {
            this.connect.unsubscribe();
        }
    }

}