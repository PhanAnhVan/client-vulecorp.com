import { Component, OnDestroy } from '@angular/core'
import { Meta, Title } from '@angular/platform-browser'
import { TranslateService } from '@ngx-translate/core'
import { Globals } from './globals'

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnDestroy {
    public connect
    public token = {
        getcompany: 'api/company'
    }
    constructor(private translate: TranslateService, private globals: Globals, private title: Title, private meta: Meta) {
        setTimeout(() => {
            this.globals.send({ path: this.token.getcompany, token: 'getcompany' })
        }, 100)
        this.translate.setDefaultLang('vi')
        this.translate.use('vi')
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getcompany':
                    let data = res.data
                    this.title.setTitle(data.name)
                    this.meta.addTag({ name: 'description', content: data.description })
                    this.meta.addTag({ name: 'keywords', content: data.keywords })
                    let shortcut = document.querySelector("[type='image/x-icon']")
                    if (shortcut && data.shortcut) {
                        shortcut.setAttribute('href', data.shortcut)
                    }

                    setTimeout(() => {
                        this.translate.set('company', data)
                    }, 100)

                    break
                default:
                    break
            }
        })
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }
}
