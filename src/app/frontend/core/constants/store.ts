export class Languages {
    public static value = []
    public static default = 'vn'
}

export const CompanyInfo = '@Company/Info'

export const SetStorage = (name: string, value) => localStorage.setItem(name, JSON.stringify(value))

export const GetStorage = (name: string) => JSON.parse(localStorage.getItem(name))

export const RemoveStorage = (name: string) => localStorage.removeItem(name)

export const handleCodeRouter = (code: string) => {
    const CODE_LIST = ['about']

    return CODE_LIST.includes(code)
}
