import { Component, OnDestroy, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { SlidesOutputData } from 'ngx-owl-carousel-o'
import { ToastrService } from 'ngx-toastr'
import { Subscription } from 'rxjs'
import { Globals } from '../../../globals'

@Component({
    selector: 'app-detail-product',
    templateUrl: './detail-product.component.html',
    styleUrls: ['./detail-product.component.scss']
})
export class DetailProductComponent implements OnInit, OnDestroy {
    public connect: Subscription

    public listImages = { data: [], cached: [], active: 0 }
    public data: any = {}
    public width: number
    public widthOwl: number
    public link: any
    public activeSlides: SlidesOutputData
    public selected: any

    public token: any = {
        getProductDetail: 'api/getProductDetail'
    }

    constructor(public route: ActivatedRoute, public router: Router, public toastr: ToastrService, public globals: Globals) {
        this.width = document.body.getBoundingClientRect().width
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'getProductDetail':
                    this.data = res.data
                    let listimages = res.data.listimages && res.data.listimages.length > 5 ? JSON.parse(res.data.listimages) : []
                    this.listImages.cached = listimages
                    if (this.data.images && this.data.images.length > 4) {
                        this.listImages.cached = [this.data.images].concat(listimages)
                    } else {
                        this.data.images = listimages.length > 0 ? listimages[0] : ''
                    }
                    this.listImages.data = Object.values(this.listImages.cached)
                    this.related.data = res.data.related

                    setTimeout(() => {
                        if (window['openfancybox']) {
                            window['openfancybox']()
                        }
                    }, 1000)
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.link = params.link
            setTimeout(() => {
                this.globals.send({
                    path: this.token.getProductDetail,
                    token: 'getProductDetail',
                    params: { link: this.link }
                })
            }, 50)
        })

        setTimeout(() => {
            this.renderHtml()
        }, 1000)

        this.widthOwl = document.getElementById('owl-carousel').offsetWidth / (this.width > 415 ? 4 : 2) - (this.width > 415 ? 15 : 20)
    }

    isActive(images: any, index: number) {
        this.data.images = images
        this.listImages.active = index
    }

    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    renderHtml = () => {
        let main = document.getElementById('contentDetail')
        if (main) {
            let el = main.querySelectorAll('table')
            7
            if (el) {
                for (let i = 0; i < el.length; i++) {
                    let div = document.createElement('div')
                    div.className = 'table-responsive table-bordered m-0 border-0'
                    el[i].parentNode.insertBefore(div, el[i])
                    el[i].className = 'table'
                    el[i].setAttribute('class', 'table')
                    let cls = el[i].getAttribute('class')
                    el[i]
                    let newhtml = "<table class='table'>" + el[i].innerHTML + '</table>'
                    el[i].remove()
                    div.innerHTML = newhtml
                }
            }
            let image = main.querySelectorAll('img')
            if (image) {
                for (let i = 0; i < image.length; i++) {
                    let a = document.createElement('div')
                    a.className = 'images-deatil d-inline'
                    image[i].parentNode.insertBefore(a, image[i])
                    let style = image[i].style.cssText
                    let src = image[i].currentSrc
                    let html =
                        `<a  class="fancybox" data-fancybox="images-preview" data-thumbs="{&quot;autoStart&quot;:true}" href="` +
                        src +
                        `">
                         <img class=""  src="` +
                        src +
                        `" style="` +
                        style +
                        `" alt="` +
                        this.data.name +
                        `">
                    </a>`
                    image[i].remove()
                    a.innerHTML = html
                }
            }
        }
    }

    public library = {
        libraryOptions: {
            loop: false,
            autoplayTimeout: 8000,
            autoplaySpeed: 1500,
            mouseDrag: true,
            touchDrag: true,
            pullDrag: false,
            dots: false,
            navSpeed: 500,
            dotsData: true,
            items: 1,
            navText: ["<i class='fas fa-angle-left'></i>", "<i class='fas fa-angle-right'></i>"],
            nav: true
        },

        getPassedData: (data: SlidesOutputData) => {
            this.activeSlides = data
            this.selected = data.slides[0] ? data.slides[0].id : ''
            this.data.images = this.selected
            setTimeout(() => {
                this.library.onClickImageChild(this.selected + '-child')
            }, 200)
        },

        onClickImageChild: (id: string) => {
            let items: any = document.querySelectorAll('.img-child')
            for (let i = 0; i < items.length; i++) {
                items[i].style.opacity = 0.5
                items[i].classList.remove('border')
                items[i].classList.remove('border-success')
            }
            document.getElementById(id).style.opacity = '1'
            document.getElementById(id).classList.add('border')
            document.getElementById(id).classList.add('border-success')
        }
    }

    related = {
        data: [],
        relatedOptions: {
            autoWidth: true,
            mouseDrag: true,
            touchDrag: true,
            pullDrag: true,
            margin: 10,
            responsive: {
                0: {
                    items: 2
                },
                700: {
                    items: 3
                },
                940: {
                    items: 5
                }
            },
            dots: false,
            nav: true,
            navText: ["<i class='fas fa-angle-left'></i>", "<i class='fas fa-angle-right'></i>"]
        }
    }
}
