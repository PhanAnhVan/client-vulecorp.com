import { Component, OnDestroy, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { Globals } from '../../globals'

@Component({
    selector: 'app-page',
    templateUrl: './page.component.html'
})
export class PageComponent implements OnInit, OnDestroy {
    public detail: any

    public data = <any>[]
    public template = <any>[]

    public connect

    public token: any = {
        getpageslink: 'api/getDetailPagesByLink'
    }
    constructor(public route: ActivatedRoute, public globals: Globals, public router: Router) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'pagesdetail':
                    this.data = res.data
                    if (!this.data || this.data.length == 0) {
                        this.router.navigate(['/404'])
                    }
                    setTimeout(() => {
                        this.renderHtml()
                    }, 500)
                    if (this.data.template) {
                        this.template = this.data.template
                    }

                    break
                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            let link = params.link
            this.globals.send({ path: this.token.getpageslink, token: 'pagesdetail', params: { link: link } })
        })
    }
    ngOnDestroy() {
        this.connect.unsubscribe()
    }
    renderHtml = () => {
        let main = document.getElementById('contentDetail')
        if (main) {
            let el = main.querySelectorAll('table')
            7
            if (el) {
                for (let i = 0; i < el.length; i++) {
                    let div = document.createElement('div')
                    div.className = 'table-responsive table-bordered m-0 border-0'
                    el[i].parentNode.insertBefore(div, el[i])
                    el[i].className = 'table'
                    el[i].setAttribute('class', 'table')
                    let cls = el[i].getAttribute('class')
                    el[i]
                    let newhtml = "<table class='table'>" + el[i].innerHTML + '</table>'
                    el[i].remove()
                    div.innerHTML = newhtml
                }
            }
            let image = main.querySelectorAll('img')
            if (image) {
                for (let i = 0; i < image.length; i++) {
                    let a = document.createElement('div')
                    a.className = 'images-deatil d-inline'
                    image[i].parentNode.insertBefore(a, image[i])
                    let style = image[i].style.cssText
                    let src = image[i].currentSrc
                    let html =
                        `<a  class="fancybox" data-fancybox="images-preview" data-thumbs="{&quot;autoStart&quot;:true}" href="` +
                        src +
                        `">
                         <img class=""  src="` +
                        src +
                        `" style="` +
                        style +
                        `" alt="` +
                        this.data.name +
                        `">
                    </a>`
                    image[i].remove()
                    a.innerHTML = html
                }
            }
        }
    }
}
