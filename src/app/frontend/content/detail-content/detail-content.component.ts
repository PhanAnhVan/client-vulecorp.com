import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Globals } from 'src/app/globals';

@Component({
    selector: 'app-detail-content',
    templateUrl: './detail-content.component.html',
    styleUrls: ['./detail-content.component.css'],
})
export class DetailContentComponent implements OnInit, OnDestroy {
    public connect: Subscription;

    public data = <any>{};
    public category = <any>[];
    public parent_link: any;
    public link = '';
    public width: number;

    public token: any = {
        getContentDetail: "api/getContentDetail",
    }

    constructor(
        public globals: Globals,
        public fb: FormBuilder,
        public route: ActivatedRoute,
        public router: Router,
    ) {
        this.width = document.body.getBoundingClientRect().width;
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case "getContentDetail":
                    this.data = res.data;
                    this.data.keywords = JSON.parse(this.data.keywords);

                    setTimeout(() => {
                        this.renderHtml();
                    }, 500);
                    break;
                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.link = params.link;
            setTimeout(() => {
                this.globals.send({ path: this.token.getContentDetail, token: "getContentDetail", params: { link: this.link } });
            }, 50);
        });

        this.globals.send({ path: this.token.getContentCategory, token: "getContentCategory" });
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }

    renderHtml = () => {
        let main = document.getElementById("contentDetail");
        if (main) {
            let el = main.querySelectorAll("table"); 7
            if (el) {
                for (let i = 0; i < el.length; i++) {
                    let div = document.createElement("div");
                    div.className = "table-responsive table-bordered m-0 border-0";
                    el[i].parentNode.insertBefore(div, el[i]);
                    el[i].className = 'table';
                    el[i].setAttribute('class', 'table');
                    let cls = el[i].getAttribute('class');
                    el[i]
                    let newhtml = "<table class='table'>" + el[i].innerHTML + "</table>";
                    el[i].remove();
                    div.innerHTML = newhtml;
                }
            }
            let image = main.querySelectorAll("img");
            if (image) {
                for (let i = 0; i < image.length; i++) {
                    let a = document.createElement("div");
                    a.className = "images-deatil d-inline";
                    image[i].parentNode.insertBefore(a, image[i]);
                    let style = image[i].style.cssText
                    let src = image[i].currentSrc
                    let html = `<a  class="fancybox" data-fancybox="images-preview" data-thumbs="{&quot;autoStart&quot;:true}" href="` + src + `">
                         <img class=""  src="`+ src + `" style="` + style + `" alt="` + this.data.name + `">
                    </a>`
                    image[i].remove()
                    a.innerHTML = html;
                }
            }
        }
    }
}