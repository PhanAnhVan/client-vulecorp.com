/**
 *
 * Create
 * Name:
 * Date:
 * Note:
 * ------------------
 * Edit
 * Name:
 * Date:
 * Note:ng 
 *
 **/

import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { Globals } from '../globals';

@Component({
    selector: 'app-frontend',
    templateUrl: './frontend.component.html',
    styleUrls: ['./frontend.component.scss'],
})

export class FrontendComponent implements OnInit {
    public connect: Subscription;

    public path = "";
    public token: any = {
        language: "api/setting/language",
    }
    constructor(
        public translate: TranslateService,
        public router: Router,
        public globals: Globals,
    ) {
        // gọi file lang lên vsf đưa vào vi.json
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case "language":
                    let data = res.data.reduce((n: { [x: string]: any; }, o: { text_key: string | number; }) => {
                        if (!n[o.text_key]) { n[o.text_key] = o } return n;
                    }, []);
                    for (let key in data) {
                        this.translate.set(key, data[key]['value']);
                    }

                    break;
                default: break;
            }
        });

        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) { window.scroll(0, 0); }
        })
    }

    ngOnInit() {
        if (window['scrolltop']) {
            window['scrolltop']();
        }
        this.globals.send({
            path: this.token.language,
            token: 'language',
            params: { status: 1 }
        });
    }
}