import { Component, OnDestroy, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { TranslateService } from '@ngx-translate/core'
import { Subscription } from 'rxjs'
import { Globals } from 'src/app/globals'

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
    private connect: Subscription
    private token = {
        productGroup: 'api/getProductGroupHome',
        productList: 'api/getProductList'
    }

    public width: number = document.body.getBoundingClientRect().width
    public widthOwl: number

    constructor(public globals: Globals, public translate: TranslateService, public router: Router) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case 'productCategory':
                    this.productCategory.data = res.data
                    break
                case 'getProductGroupHome':
                    this.product.data = res.data
                    break

                case 'getbanner':
                    this.slide.data = res.data
                    break

                case 'getAboutUs':
                    this.aboutUs.data = res.data
                    break

                case 'getContentHome':
                    this.content.data = res.data
                    break

                case 'getPartner':
                    this.partner.data = res.data
                    break

                default:
                    break
            }
        })
    }

    ngOnInit() {
        this.product.send()
        this.aboutUs.send()
        this.content.send()
        this.slide.send()
        this.partner.send()
        this.productCategory.send()
        this.widthOwl = document.getElementById('owl-carousel').offsetWidth / (this.width > 415 ? 5 : 2) - (this.width > 415 ? 15 : 20)
    }
    ngOnDestroy() {
        this.connect.unsubscribe()
    }

    public lengthItem = 0

    public product = {
        data: [],
        send: () => {
            this.globals.send({ path: this.token.productGroup, token: 'getProductGroupHome' })
        }
    }

    public productCategory = {
        token: 'api/productCategory',
        data: [],
        send: () => {
            this.globals.send({
                path: this.productCategory.token,
                token: 'productCategory'
            })
        }
    }

    public aboutUs = {
        token: 'api/getAboutUs',
        data: <any>{},
        send: () => {
            this.globals.send({ path: this.aboutUs.token, token: 'getAboutUs' })
        }
    }

    public content = {
        token: 'api/getContentHome',
        data: <any>{},
        send: () => {
            this.globals.send({
                path: this.content.token,
                token: 'getContentHome',
                params: { limit: 4 }
            })
        }
    }

    public partner = {
        token: 'api/getPartner',
        data: <any>[],
        send: () => {
            this.globals.send({ path: this.partner.token, token: 'getPartner' })
        },
        options: {
            autoWidth: true,
            loop: true,
            mouseDrag: true,
            touchDrag: true,
            pullDrag: false,
            margin: 10,
            responsive: {
                0: {
                    items: 2
                },
                415: {
                    items: 4
                },
                940: {
                    items: 8
                }
            },
            dots: false,
            nav: true,
            navText: ["<i class='fas fa-angle-left'></i>", "<i class='fas fa-angle-right'></i>"]
        }
    }
    /** Banner **/
    public slide = {
        token: 'api/home/slide',
        data: [],
        send: () => {
            this.globals.send({ path: this.slide.token, token: 'getbanner', params: { type: 2 } })
        }
    }
}
