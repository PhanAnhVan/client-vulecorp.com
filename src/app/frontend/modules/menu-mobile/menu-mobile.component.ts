import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '../../../../../node_modules/@angular/router';
import { Globals } from '../../../globals';

@Component({
    selector: 'app-menu-mobile',
    templateUrl: './menu-mobile.component.html',
    styleUrls: ['./menu-mobile.component.css']
})
export class MenuMobileComponent implements OnInit, OnDestroy {
    @Output('menumobile') menumobile = new EventEmitter<number>();

    public connect: Subscription;

    public show: number;
    public url: string = '';
    public data: any;
    public menu: any;

    public token: any = {
        menu: "api/getmenu",
    }

    constructor(
        public globals: Globals,
        public router: Router,
    ) {
        this.connect = this.globals.result.subscribe((res: any) => {
            switch (res.token) {
                case "getmenu":
                    let data = this.compaid(res.data);
                    this.menu = data;
                    break;

                default:
                    break;
            }
        });
    }

    ngOnInit() {
        this.globals.send({
            path: this.token.menu,
            token: 'getmenu',
            params: { position: 'menuMain' }
        });
    }

    compaid(data: any[]) {
        let list = [];

        data = data.filter(function (item: { parent_id: string | number; }) {
            let v = (isNaN(+item.parent_id) && item.parent_id) ? 0 : +item.parent_id;
            v == 0 ? '' : list.push(item);
            return v == 0 ? true : false;
        })

        let compaidmenu = (data: string | any[], skip: boolean, level = 0) => {
            level = level + 1;
            if (skip == true) {
                return data;
            } else {
                for (let i = 0; i < data.length; i++) {
                    let obj = [];
                    list = list.filter(item => {
                        let skip = (+item.parent_id == +data[i]['id']) ? false : true;
                        if (skip == false) { obj.push(item); }
                        return skip;
                    })
                    let skip = (obj.length == 0) ? true : false;
                    data[i]['href'] = getType(data[i]['link'], +data[i].type);
                    data[i]['level'] = level;
                    data[i]['data'] = compaidmenu(obj, skip, level);
                }
                return data;
            }
        };
        let getType = (link: any, type: number) => {
            switch (+type) {
                case 1:
                case 2:
                case 3:
                    link = link
                    break;
                case 4:
                    link = link
                    break;
                default:
                    break;
            }
            return link;
        }
        return compaidmenu(data, false);

    }

    showMenuChild(skip: any, item: { data: string | any[]; id: string; level: number; href: string; }) {
        if (item.data && item.data.length > 0) {

            let elm = document.getElementById('dropdown-menu-child-' + item.id);

            skip ? elm.classList.add("active-menu-child") : (elm.classList.remove("active-menu-child"))

        } else {

            let elm2 = document.getElementById("menu-mobi");

            elm2.classList.add("menu-mobi-hidden");

            elm2.classList.remove("menu-mobi-block");

            this.menumobile.emit();

            if (skip) {

                let elm = document.getElementsByClassName('menu-child');
                elm[0].classList.remove("active-menu-child")
            }

            this.router.navigate([(item.level == 3) ? ('/san-pham/' + item.href) : ('/' + item.href)]);
        }
    }

    ngOnDestroy() {
        this.connect.unsubscribe();
    }
}