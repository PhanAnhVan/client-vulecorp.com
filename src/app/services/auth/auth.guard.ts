import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Router } from '@angular/router';
import { Globals } from "../../globals";
import { Observable } from 'rxjs';
@Injectable()
export class UserAuthGuard implements CanActivate {
    constructor(private router: Router, private globals: Globals) { }
    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {            
        if (this.globals.USERS.check(true) ) {
            let skip = true
            this.globals.USERS._check().subscribe((res: any) => {
                skip = res.skip
                if (res.skip == false) {
                   this.router.navigate(['/login']);
                    return false;
                } else {
                    this.globals.USERS.set(res.data, true);
                    return true;
                }
            });
            return skip;
        } else {
            this.router.navigate(['/login']);
            return false;
        }
    }
}
