import { Injectable } from '@angular/core'
import { Subject } from 'rxjs'
import { Router } from '@angular/router'
import { HttpClient } from '@angular/common/http'

export class configOptions {
    path: string
    data?: Object
    params?: Object
    token: string
}

@Injectable()
export class Globals {
    public BASE_API_URL = 'https://vulecorp.com/'
    // public BASE_API_URL = 'http://localhost/projects/vu_le_corp/server/'

    public admin: string = 'admin'
    public debug: Boolean = false
    private res = new Subject<string>()
    public company: any = {}
    public result = this.res.asObservable()
    public configCkeditor = {
        filebrowserBrowseUrl: this.BASE_API_URL + 'public/ckfinder/ckfinder.html',
        filebrowserImageBrowseUrl: this.BASE_API_URL + 'public/ckfinder/ckfinder.html?type=Images',
        filebrowserFlashBrowseUrl: this.BASE_API_URL + 'public/ckfinder/ckfinder.html?type=Flash',
        filebrowserUploadUrl: this.BASE_API_URL + 'public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
        filebrowserImageUploadUrl: this.BASE_API_URL + 'public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
        filebrowserFlashUploadUrl: this.BASE_API_URL + 'public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
    }

    constructor(private http: HttpClient, public router: Router) {
        this.result.subscribe((res: any) => {
            if (this.debug) {
                console.log(res)
            }
        })
    }
    send = (option: configOptions) => {
        if (option.path && option.token) {
            // kiểm tra token
            let params = () => {
                let param = '?mask=' + option.token
                if (option.params) {
                    let keys = Object.keys(option.params)
                    for (let i = 0; i < keys.length; i++) {
                        if (i == 0) {
                            param += '&'
                        }
                        param += keys[i] + '=' + option.params[keys[i]]
                        param += i + 1 == keys.length ? '' : '&'
                    }
                }
                return param
            }
            this.http.post(this.BASE_API_URL + option.path + params(), option.data).subscribe((result: any) => {
                this.res.next(result)
            })
        }
    }
    public time = {
        format: () => {},
        date: e => {
            e = typeof e === 'object' ? e : new Date()
            return e.getFullYear() + '-' + (e.getMonth() + 1).toString() + '-' + e.getDate()
        }
    }

    public price = {
        format: (price: { toString: () => string }) => {
            return typeof price == 'number' ? price : +price.toString().replace(/\./g, '')
        },
        change: (price: any) => {
            let value = price.toString().replace(/\./g, '')
            let val = isNaN(value) ? 0 : +value
            let res = isNaN(value) ? 0 : val < 0 ? 0 : value
            return (price = Number(res).toLocaleString('vi'))
        }
    }

    public USERS = {
        token: 'users',
        store: 'localStorage',
        _check: () => {
            return this.http.post(this.BASE_API_URL + 'api/login/check', this.USERS.get(true))
        },
        get: skip => {
            if (skip == true) {
                return window.localStorage.getItem(this.USERS.store) ? JSON.parse(window.localStorage.getItem(this.USERS.store)) : {}
            } else {
                return window.localStorage.getItem(this.USERS.token) ? window.localStorage.getItem(this.USERS.token) : null
            }
        },
        check: skip => {
            return skip == true
                ? window.localStorage.getItem(this.USERS.store)
                    ? true
                    : false
                : window.localStorage.getItem(this.USERS.token)
                ? true
                : false
        },
        set: (data, skip) => {
            data = typeof data === 'object' ? JSON.stringify(data) : data
            if (skip == true) {
                window.localStorage.setItem(this.USERS.store, data)
            } else {
                window.localStorage.setItem(this.USERS.token, data)
            }
        },
        remove: (skip: any = '') => {
            if (!skip) {
                window.localStorage.clear()
            } else {
                if (skip == true) {
                    window.localStorage.removeItem(this.USERS.store)
                } else {
                    window.localStorage.removeItem(this.USERS.token)
                }
            }
            this.router.navigate(['/login/'])
            return this.http.post(this.BASE_API_URL + 'api/logout/admin', {}).subscribe((result: any) => {
                this.res.next(result)
            })
        }
    }
}
