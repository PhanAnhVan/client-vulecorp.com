function viewLibrary() {
    $('.fancybox').fancybox({
        thumbs: { autoStart: true }
    })
    $('[data-fancybox]').fancybox({
        thumbs: { autoStart: true }
    })
}
$(document).ready(function () {
    $('body').tooltip({ selector: '[data-toggle=tooltip]' })
})

function fixmenu() {
    $(window).on('scroll', function () {
        if ($(window).scrollTop() > 200) {
            $('.menu').addClass('navbar-fixed')
        } else {
            $('.menu').removeClass('navbar-fixed')
        }
    })
}

function scrolltop() {
    $(window).on('scroll', function () {
        if ($(this).scrollTop() > 200) {
            $('.scroll-top').fadeIn(200)
        } else {
            $('.scroll-top').fadeOut(200)
        }
    })
    $('.scroll-top').on('click', function () {
        $('html,body').animate({ scrollTop: 0 }, 1500)
        return false
    })
}

function libraryGallary() {
    var $container = $('.animate-grid .gallary-thumbs')
    $container.isotope({
        filter: '*',
        animationOptions: {
            duration: 750,
            easing: 'linear',
            queue: false
        }
    })
    $('.animate-grid .categories a').click(function () {
        $('.animate-grid .categories .active').removeClass('active')
        $(this).addClass('active')
        var selector = $(this).attr('data-filter')
        $container.isotope({
            filter: selector,
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        })
        return false
    })
}

/*  
var _Hasync = _Hasync || [];
function hisTats() {
    _Hasync.push(['Histats.start', '1,4527274,4,336,112,62,00011111']);
    _Hasync.push(['Histats.fasi', '1']);
    _Hasync.push(['Histats.track_hits', '']);
    (function () {
        var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
        hs.src = ('//s10.histats.com/js15_as.js');
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
    })();

    setTimeout(() => {
        let length = document.querySelectorAll('img').length;
        document.querySelectorAll('img')[length - 1].style.display = "none";
        document.querySelectorAll('img')[length - 2].style.display = "none";
    }, 4000);
}
*/
